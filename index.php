<?php

header("Content-Type:application/json");

$productos = array
(
 array  ("1", "Tecate Roja", "44", "TecateRoja.png" ),
 array  ("2", "Miller", "45", "Miller.jpg" ),
 array  ("3", "Indio", "47", "Indio.jpg" ),
 array  ("4", "Corona", "49", "Corona.png" ),
 array  ("5", "Tecate Azul", "30", "TecateAzul.jpg" ),
 array  ("6", "Amstel", "44", "amstel.png" ),
 array  ("7", "Ultra", "45", "ultra.jpg" ),
 array  ("8", "Budlight", "47", "budlight.jpg" ),
 array  ("9", "Budweiser", "49", "budweiser.png" ),
 array  ("10", "Carta Blanca", "30", "cartablanca.jpg" )
);


if(!empty($_GET["codigo"]))
{
   $codigo = $_GET ["codigo"];    
   //peticion(200, "El producto a buscar es:".$codigo);
   $bandera = false;
   for ($i=0; $i < 10; $i++) { 
      if ($productos [$i][0] == $codigo) {
        peticion(200, $productos[$i][1], $productos[$i][2], $productos[$i][3]);
        $bandera=true;
      }
      
   }
   if ($bandera==false) {
      peticion(200,NULL,NULL,NULL);
   }
}
else{
 peticion(400,NULL,NULL,NULL);
}


function peticion($status, $data, $precio, $imagen){
   header("HTTP/1.1".$status);
   $respuesta["Status"] = $status;
   $respuesta["Nombre"] = $data;
   $respuesta["Precio"] = $precio;
   $respuesta["Imagen"] = $imagen;

   $json_response = json_encode($respuesta);
   echo $json_response;
}
?>